 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fm" %>

<div class="container" style="margin-bottom: 10px ; width: 600px" >
		<h1>Gestion des paniers :</h1>
		    <c:if test="${not empty requestScope.error}">
			  <div class="alert alert-danger">
			  <strong>Danger!</strong> ${requestScope.error}
			</div>
			</c:if>
		
		<c:if test="${  requestScope.panier._iD == 0}">
		 <form action='${pageContext.request.contextPath}/Panier?action=Add' method="post">
	    </c:if>
	    
	    <c:if test="${ requestScope.panier._iD !=0 }">
		 <form action='${pageContext.request.contextPath}/Panier?action=modify' method="post">
	    </c:if>
	  <div class="form-group">
	    <label for="2">date de cr�ation : </label>
	    <input name="date" type="date" class="form-control" id="2" required="required"    value="<fm:formatDate value="${requestScope.panier._date_creation}" pattern="YYYY-MM-dd"/>" >
	  </div>
	 
	  
	  <input name="id" type="hidden" value="${requestScope.panier._iD}">
	  
	  <input name="add" type="submit" class="btn"  required="required">
	  
	</form>
</div>
