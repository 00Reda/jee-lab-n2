 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fm" %>
 <!-- 
<c:out value='${param.nom}'></c:out> 
<c:if test='${!empty param.nom}'>
<jsp:useBean id="produit" class="ma.fstt.entities.Produit" scope="request"></jsp:useBean>
<jsp:setProperty property="*" name="produit"/>
<c:out value="${produit._nom }"></c:out>
<jsp:forward page="${pageContext.request.contextPath}/Produit?action=add"></jsp:forward>
</c:if>
 -->
<div class="container" style="margin-bottom: 10px ; width: 600px" >
		<h1>Gestion des produits :</h1>
		    <c:if test="${not empty requestScope.error}">
			  <div class="alert alert-danger">
			  <strong>Danger!</strong> ${requestScope.error}
			</div>
			</c:if>
		
		<c:if test="${  requestScope.produit._iD == 0}">
		 <form action='${pageContext.request.contextPath}/Produit?action=Add' method="post">
	    </c:if>
	    
	    <c:if test="${ requestScope.produit._iD !=0 }">
		 <form action='${pageContext.request.contextPath}/Produit?action=modify' method="post">
	    </c:if>
	  <div class="form-group">
	    <label for="2">nom : </label>
	    <input name="nom" type="text" class="form-control" id="2" placeholder="laptop" required="required" value="${requestScope.produit._nom}">
	  </div>
	  <div class="form-group">
	    <label for="3">modele : </label>
	    <input name="modele" type="text" class="form-control" id="3" placeholder="ideapad 320s" required="required" value="${requestScope.produit._modele}">
	  </div>
	 
	  <div class="form-group">
	    <label for="4">quantite : </label>
	    
	    <input name="quantite" type="number" min="0" max="9999" class="form-control" id="4" placeholder="5" required="required" value="${requestScope.produit._quantite}">
	  </div>
	  <div class="form-group">
	    <label for="4">prix : </label>
	    
	    <input name="prix" type="number" min="0.00" class="form-control" id="4" placeholder="5" required="required" value="${requestScope.produit._prix}">
	  </div>
	   <div class="form-group">
	    <label for="4">photo : </label>
	    
	    <input name="photo" type="text" class="form-control" id="4" placeholder="5" required="required" value="${requestScope.produit.photo}">
	  </div>
	  
	  
	  <input name="id" type="hidden" value="${requestScope.produit._iD}">
	  
	  <input name="add" type="submit" class="btn"  required="required">
	  
	</form>
</div>
