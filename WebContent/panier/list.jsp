<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"
    %>
<jsp:include page="../include/header.jsp"></jsp:include>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<body>

 <div class="container">
 
    <h1>liste des paniers !</h1>
    <div class="container">
 		
	    <!-- Button trigger modal -->
		<a href="${pageContext.request.contextPath}/Panier?action=Add" class="btn btn-primary">ajout
		</a>
		
    </div>
    
    <c:if test="${not empty requestScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${requestScope.error}"></c:out>
	</div>
	</c:if>
	
	<c:if test="${not empty requestScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${requestScope.msg}"></c:out>
	</div>
	</c:if>
	
	 <c:if test="${not empty sessionScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${sessionScope.error}"></c:out>
	  <c:remove var="error" scope="session"/>
	</div>
	</c:if>
	
	<c:if test="${not empty sessionScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${sessionScope.msg}"></c:out>
	  <c:remove var="msg" scope="session"/>
	</div>
	</c:if>
      
    <c:if test="${ fn:length(requestScope.list) > 0}">
	    <table class="table" style="margin:7px">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">date de creation</th>
	      <th scope="col">des produits </th>
	      <th scope="col">les actions </th>
	     
	    </tr>
	  </thead>
	  <tbody>
	  <c:forEach items="${requestScope.list}" var="panier">
	    <tr>
	      <th scope="row">${panier._iD}</th>
	      <td>
	      
	           <fm:formatDate value="${panier._date_creation}" pattern="dd-MM-yyyy"/>
	         
	      </td>
	      <td>
	         <a href="${pageContext.request.contextPath}/Panier?action=loadproducts&id=${panier._iD}" class="btn btn-info productLoader" role="button" >voir</a>
	         <a href="${pageContext.request.contextPath}/Produit?action=list" class="btn btn-info productaffectation" role="button" data-id='${panier._iD}'>ajouter</a>
	      </td>
	      <td>
	           <a href="${pageContext.request.contextPath}/Panier?action=modify&id=${panier._iD}" class="btn btn-info" role="button">Modifier</a>
	           <a href="${pageContext.request.contextPath}/Panier?action=delete&id=${panier._iD}" onclick="return confirm('vous etes sure de vouloir supprimer :')" class="btn btn-info" role="button">SUPP</a>
	      </td>
	      
	      
	    </tr>
	    </c:forEach>
	  </tbody>
	</table>
	</c:if>
	
	
</div>
		
<div class="modal bd-example-modal-lg" tabindex="-1" role="dialog" id='products' style='width: 100%' aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">liste des produits du panier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="table-responsive"> 
		  <div class='container' id='alertDeleteProduct'>
		  </div>
        <table class="table" style="margin:7px">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">nom</th>
	      <th scope="col">modele</th>
	      <th scope="col">prix</th>
	      <th scope="col">quantite en stock</th>
	      <th scope="col">quantite demandé</th>
	      <th scope="col">photo</th>
	      <th scope="col">action</th>
	    </tr>
	  </thead>
	  <tbody>
	 		  
	  </tbody>
	</table>
	  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal bd-example-modal-lg" tabindex="-1" role="dialog" id='affproduct' style='width: 100%' aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Affectation d'un produit :</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="table-responsive"> 
		  <div class='container' id='alertAffectProduct'>
		  </div>
             <form action='${pageContext.request.contextPath}/Panier?action=affectation&id=' method="post" id='submitProductForm'>
	     
			  <div class="form-group">
			    <label for="select">produit ( nom / modele / quantite en stock / prix ) : </label>
			    <select name="produit" class="form-control" id="select"  required="required" >
			    </select>
			  </div>
			  
			  <div class="form-group">
			    <label for="3">quantité a demander : </label>
			    <input name="quantite" type="number" class="form-control" id="3" min="1.0" max="50" required="required" placeholder="1">
			  </div>
			  
			  <input value="ajouter" type="submit" id='submitProduct' class='btn btn-info'>
			  
			</form>
	  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<jsp:include page="../include/footer.jsp"></jsp:include>
</body>
</html>