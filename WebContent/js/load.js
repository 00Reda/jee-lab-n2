
$(document).ready(function(){
	
	$(".productLoader").on('click',

	        function(event){
	            event.preventDefault();
	            
	            $.ajax({
	            	 url:this.href,
	            	 success:function(result){
	            		 console.log(result);
	            		 var html='';
	            		for(var i=0;i<result.length;i++){
	            			
	            			html+='<tr id="'+result[i]._produits._iD+'">'+
	            		      '<th scope="row">'+result[i]._produits._iD+'</th>'+
	            		      '<td>'+result[i]._produits._nom+'</td>'+
	            		      '<td>'+result[i]._produits._modele+'</td>'+
	            		      '<td>'+result[i]._produits._prix+'</td>'+
	            		      '<td>'+result[i]._produits._quantite+'</td>'+
	            		      '<td>'+result[i].quantite+'</td>'+
	            		      '<td><img class="img-thumbnail" src="'+result[i]._produits.photo+'"> </td>'+
	            		      '<td><a  href="/atelier_jee_2/Panier?action=deleteProduct&id='+result[i].id+'"  class="btn btn-info delete-product" role="button">SUPP</a></td>'+
	            		      '</tr>';
	            			
	            		}
	            		
	            		$("#products table tbody").html(html);
	            		addDeleteProductEvent();
	            		$('#products').modal("show");
	            		
	            	 },
	            	 error:function(error){
						  console.log(error);
						  var div=$(".container")[1];
						  var html=div.innerHTML;
						  div.innerHTML=html+"<div class='alert alert-danger'>"+
						 " <strong>erreur</strong> "+error.responseText+
						  "</div>";
	            	 }
	            })
	        }
	);

	$(".productaffectation").on("click",function(event){
	           
		event.preventDefault();
		this.innerHTML='loading...';
		elm=this;
		$.ajax({
			
			 url:this.href,
			 
			 success:function(result){
				 var items=JSON.parse(result);
				 $('#select').html("");
				 $.each(items,function(i,item){
					 
					 $('#select').append($('<option>',{
						 value:item._iD,
						 text:item._nom+" / "+item._modele+" / "+item._quantite+" / "+item._prix
					 }))
				 })
				 
				 elm.innerHTML="ajouter";
				 var act=$("#submitProductForm").attr('action');
			 
					 $("#submitProductForm").attr('action', ( act.substr(0,act.indexOf("id=")+3) )+$(elm).attr("data-id"));
				 
				 $('#affproduct').modal("show");
			 },
			 
			 error:function(error){
				 console.log(error)
				 elm.innerHTML="<span style='color:red'>erreur</span>";
				 setTimeout(function(){
					 elm.innerHTML="ajouter";
				 },1000)
				 
			 }
		})
		
		
	})
	
	$("#submitProductForm").on("submit",function(e){
 
		e.preventDefault();

		$.ajax({
		    url: this.action,
		    dataType: 'text',
		    type: 'post',
		    contentType: 'application/x-www-form-urlencoded',
		    data: $(this).serialize(),
		    success: function( result ){
		       $("#alertAffectProduct").html("<div class='alert alert-success'>"+
				 " <strong>erreur</strong> "+result+
				  "</div>");
		       $('#affproduct').modal("hide");
		       $("#alertAffectProduct").html("");
		    },
		    error: function( error ){
		    	 $("#alertAffectProduct").html("<div class='alert alert-danger'>"+
				 " <strong>erreur</strong> "+error.responseText+
				  "</div>");
		    }
		});
		 
		
	})
	
})

var elm ;

var addDeleteProductEvent=function(){
	
	    
	    
		$('.delete-product').on("click",function(event){
				
				event.preventDefault();
				elm=this;
				var test=confirm("confirmer la suppression :")
				if(test==false) return ;
				
             $.ajax({
            	 
            	 url:this.href,
            	 
            	 success:function(result){
            		 $("#alertDeleteProduct").html(" <div class='alert alert-success alert-dismissible fade show' style='margin:10px' id='alertDeleteProduct'>"+result+"</div>");
            		 $(elm.parentElement.parentElement).fadeOut("slow",function(){
            			 $(this).remove();
            		 })
            	 },
            	 error:function(error){
            		 
            		 $("#alertDeleteProduct").html(" <div class='alert alert-danger alert-dismissible fade show' style='margin:10px' id='alertDeleteProduct'>"+error.responseJSON+"</div>");
            		
            	 }
             })
 				
			})
}


