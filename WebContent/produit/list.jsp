<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"
    %>
<jsp:include page="../include/header.jsp"></jsp:include>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<body>

 <div class="container">
 
    <h1>liste des produits !</h1>
    <div class="container">
 		
	    <!-- Button trigger modal -->
		<a href="${pageContext.request.contextPath}/Produit?action=Add" class="btn btn-primary">ajout
		</a>
		
    </div>
    
    <c:if test="${not empty requestScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${requestScope.error}"></c:out>
	</div>
	</c:if>
	
	<c:if test="${not empty requestScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${requestScope.msg}"></c:out>
	</div>
	</c:if>
	
	 <c:if test="${not empty sessionScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${sessionScope.error}"></c:out>
	  <c:remove var="error" scope="session"/>
	</div>
	</c:if>
	
	<c:if test="${not empty sessionScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${sessionScope.msg}"></c:out>
	  <c:remove var="msg" scope="session"/>
	</div>
	</c:if>
      
    <c:if test="${ fn:length(requestScope.list) > 0}">
	    <table class="table" style="margin:7px">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">nom</th>
	      <th scope="col">modele</th>
	      <th scope="col">prix</th>
	      <th scope="col">quantite</th>
	      <th scope="col">photo</th>
	      <th scope="col">action</th>
	    </tr>
	  </thead>
	  <tbody>
	  <c:forEach items="${requestScope.list}" var="produit">
	    <tr>
	      <th scope="row">${produit._iD}</th>
	      <td>${produit._nom }</td>
	      <td>${produit._modele}</td>
	      <td>${produit._prix}</td>
	      <td>${produit._quantite}</td>
	      <td><img class="img-thumbnail" src="${produit.photo}"> </td>
	      <td>
	           <a href="${pageContext.request.contextPath}/Produit?action=modify&id=${produit._iD}" class="btn btn-info" role="button">Modifier</a>
	           <a href="${pageContext.request.contextPath}/Produit?action=delete&id=${produit._iD}" onclick="return confirm('vous etes sure de vouloir supprimer :')" class="btn btn-info" role="button">SUPP</a>
	      </td>
	      
	      
	    </tr>
	    </c:forEach>
	  </tbody>
	</table>
	</c:if>
	
	
</div>
		


<jsp:include page="../include/footer.jsp"></jsp:include>
</body>
</html>