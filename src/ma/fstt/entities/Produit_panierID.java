package ma.fstt.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class Produit_panierID implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id",unique=true,nullable=false)
	private Panier id;

	@ManyToOne
	@JoinColumn(name="id_panier")
	private Panier _paniers;
	
	
	@ManyToOne
	@JoinColumn(name="id_produit")
	private Produit _produits;

	
	
	

	public Produit_panierID() {
		super();
	}


	public Produit_panierID(Panier id, Panier _paniers, Produit _produits) {
		super();
		this.id = id;
		this._paniers = _paniers;
		this._produits = _produits;
	}


	public Panier getId() {
		return id;
	}


	public void setId(Panier id) {
		this.id = id;
	}


	public Panier get_paniers() {
		return _paniers;
	}


	public void set_paniers(Panier _paniers) {
		this._paniers = _paniers;
	}


	public Produit get_produits() {
		return _produits;
	}


	public void set_produits(Produit _produits) {
		this._produits = _produits;
	}


	 @Override
	  public int hashCode()
	  {
		return 0;
	  }
	 
	  @Override
	  public boolean equals( Object obj )
	  {
		return false;
	  }
	
	
}
