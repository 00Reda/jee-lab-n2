
package ma.fstt.entities;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
@ApplicationScoped
@Entity
@Table(name="Produit_panier")

public class Produit_panier implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id",unique=true,nullable=false)
	private long id;

	
	@ManyToOne
	@JoinColumn(name="id_panier")
	private Panier _paniers;
	
	
	@ManyToOne
	@JoinColumn(name="id_produit")
	@Expose(serialize=false)
	private Produit _produits;
	
	@Column(name="quantite")
	private int quantite;

	
	public Produit_panier() {
		super();
	}

	
	
	public Produit_panier(long id, Panier _paniers, Produit _produits, int quantite) {
		super();
		this.id = id;
		this._paniers = _paniers;
		this._produits = _produits;
		this.quantite = quantite;
	}



	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Panier get_paniers() {
		return _paniers;
	}


	public void set_paniers(Panier _paniers) {
		this._paniers = _paniers;
	}


	public Produit get_produits() {
		return _produits;
	}


	public void set_produits(Produit _produits) {
		this._produits = _produits;
	}


	public int getQuantite() {
		return quantite;
	}


	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	
	
	



	 
}