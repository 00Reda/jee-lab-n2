package ma.fstt.entities;


import java.io.Serializable;
import java.util.Set;
import java.util.Vector;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name="produit")
@ApplicationScoped
public class Produit implements Serializable{
	
	
	@Id
	@Column(name="id_produit" , unique=true , nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int _iD;
	private String _nom;
	private String _modele;
	private String _desc;
	private float _prix;
	private int _quantite;
	private String photo ;
	
	
	@OneToMany(mappedBy="_produits")
	
	@JsonIgnore	
	private Set<Produit_panier> paniers ;
	
	
	public Produit() {
		super();
	}
	
	public Produit(int _iD, String _nom, String _modele, String _desc, float _prix, int _quantite,String photo,Set<Produit_panier> paniers) {
		super();
		this._iD = _iD;
		this._nom = _nom;
		this._modele = _modele;
		this._desc = _desc;
		this._prix = _prix;
		this._quantite = _quantite;
		this.photo=photo;
	}
	public int get_iD() {
		return _iD;
	}
	public void set_iD(int _iD) {
		this._iD = _iD;
	}
	public String get_nom() {
		return _nom;
	}
	public void set_nom(String _nom) {
		this._nom = _nom;
	}
	public String get_modele() {
		return _modele;
	}
	public void set_modele(String _modele) {
		this._modele = _modele;
	}
	public String get_desc() {
		return _desc;
	}
	public void set_desc(String _desc) {
		this._desc = _desc;
	}
	public float get_prix() {
		return _prix;
	}
	public void set_prix(float _prix) {
		this._prix = _prix;
	}
	public int get_quantite() {
		return _quantite;
	}
	public void set_quantite(int _quantite) {
		this._quantite = _quantite;
	}

	@JsonIgnore	
	public Set<Produit_panier> getPaniers() {
		return paniers;
	}
	@JsonIgnore	
	public void setPaniers(Set<Produit_panier> paniers) {
		this.paniers = paniers;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
	
	
}