package ma.fstt.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.Vector;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
@ApplicationScoped 
@Entity
@Table(name="panier")
public class Panier {
	
	@Id
	@Column(name="id_panier" , unique=true , nullable=false )
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int _iD;
	
	private Date _date_creation;
	
	@OneToMany(mappedBy="_paniers")
	@JsonIgnore
	private Set<Produit_panier> produits;
	
	public Panier() {
		super();
	}
	
	public Panier(int _iD, Date _date_creation,Set<Produit_panier> produits) {
		super();
		this._iD = _iD;
		this._date_creation = _date_creation;
		this.produits=produits;
	}
	
	public int get_iD() {
		return _iD;
	}
	public void set_iD(int _iD) {
		this._iD = _iD;
	}
	public Date get_date_creation() {
		return _date_creation;
	}
	public void set_date_creation(Date _date_creation) {
		this._date_creation = _date_creation;
	}

	public Set<Produit_panier> getProduits() {
		return produits;
	}

	public void setProduits(Set<Produit_panier> produits) {
		this.produits = produits;
	}
	
	
	
	
	
	
}