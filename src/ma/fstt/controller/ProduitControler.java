package ma.fstt.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import ma.fstt.entities.Produit;

/**
 * Servlet implementation class Produit
 */
@WebServlet("/Produit")
public class ProduitControler extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
    EntityManager manager;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProduitControler() {
		// TODO Auto-generated constructor stub
        super();
        
	} 

    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	      
		
	    /*  Produit produit=new Produit(0, "jpa", "jee", "....", 124, 40,"https://picsum.photos/200/300/?random",null);
	      
	     // EntityManagerFactory emf =
	     //         (EntityManagerFactory)getServletContext().getAttribute("emf");
	      
	      EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	      EntityManager manager=emf.createEntityManager();
	     try {
	    	 manager.getTransaction().begin();
		      manager.persist(produit);
		      manager.getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
		}*/
		
		String action=request.getParameter("action");
		if(action==null) action="list";
		switch (action) {
		case "add":
			      this.add(request,response);
			break;
		case "Add":
				  this.add(request,response);
			break;
		case "modify":
			      this.modify(request, response);
			break;
		case "delete":
			 	  this.delete(request, response);
			break;
		case "list":
		      this.list(request, response);
		break;

		default:
			break;
		}   
	      
	      
	      
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private void add(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
        manager=emf.createEntityManager();
	      
	      if(request.getMethod().equals("GET")) {
	    	   
	    	   request.setAttribute("produit", new Produit());
			   this.getServletContext().getRequestDispatcher("/produit/Add.jsp").forward(request, response);
	    	   return ;
	      }
	      //Produit produit =(Produit)request.getAttribute("produit");
	      Produit produit=new Produit();
	      produit.set_nom((String) request.getParameter("nom"));
	      produit.set_modele((String) request.getParameter("modele"));
	      String p=(String) request.getParameter("prix");
	     
	      produit.set_prix(Float.parseFloat(p));
	      p=(String) (String) request.getParameter("quantite");
	      produit.set_quantite(Integer.parseInt(p));
	      produit.setPhoto((String) request.getParameter("photo"));
	                  
	     try {
	    	 manager.getTransaction().begin();
		      manager.persist(produit);   
		      manager.getTransaction().commit();
		      request.getSession().setAttribute("msg","produit ajouter avec succee ! ");
			  response.sendRedirect("Produit?action=list");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("Produit?action=list");
		}
		
	}
	
	private void list(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
        manager=emf.createEntityManager();
        String headerName = request.getHeader("x-requested-with");
	     try {
	    	 
	    	 ArrayList<Produit>list=(ArrayList<Produit>) manager.createQuery("select p from Produit p",Produit.class).getResultList();
		    
	    	
	    	 if(null == headerName){
	    		 request.setAttribute("list", list);
			     this.getServletConfig().getServletContext().getRequestDispatcher("/produit/list.jsp").forward(request, response);
	         }
	         else {
	        	 ObjectMapper mapper = new ObjectMapper();
		    	 String jsonWriter = mapper.writerWithDefaultPrettyPrinter()
		    			 .writeValueAsString(list);
			       
			     response.getOutputStream().print(jsonWriter);
			     return ;
	         }
	    	
		     
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			
			
			if(null == headerName){
				request.getSession().setAttribute("error",e.getMessage());
				response.sendRedirect("produit/list.jsp");
	         }
	         else {
	        	 response.setStatus(400);
	        	 ObjectMapper mapper = new ObjectMapper();
		    	 String jsonWriter = mapper.writerWithDefaultPrettyPrinter()
		    			 .writeValueAsString(e.getMessage());
			       
			     response.getOutputStream().print(jsonWriter);
			     return ;
	         }
		}
		
	}

	
	private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
        manager=emf.createEntityManager();
		  
	      Produit produit=new Produit();
	      String id=request.getParameter("id");
	      produit.set_iD(Integer.parseInt(id));
	      
	     try {
	    	  manager.getTransaction().begin();
		    
			
		      produit=manager.merge(produit);
		      manager.remove(produit);
		      request.getSession().setAttribute("msg","produit supprime avec succee !!");
		      
		      manager.getTransaction().commit();
		      
		      
		      ArrayList<Produit>list=(ArrayList<Produit>) manager.createQuery("select p from Produit p").getResultList();
		      request.setAttribute("list", list);
		      this.getServletConfig().getServletContext().getRequestDispatcher("/produit/list.jsp").forward(request, response);
		      
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("Produit?action=list");
		}
		
	}
	
	private void modify(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		
		
		  EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	      EntityManager manager=emf.createEntityManager();
	      
	      String id=request.getParameter("id");
	      
	      if(id==null) return;
	      
	      if(request.getMethod().equals("GET")) {
	    	   manager.getTransaction().begin();
	    	   Produit p=manager.find(Produit.class, Integer.parseInt(id));
	    	   request.setAttribute("produit", p);
			   this.getServletContext().getRequestDispatcher("/produit/Add.jsp").forward(request, response);
	    	   return ;
	      }
	      //Produit produit =(Produit)request.getAttribute("produit");
	      
	                  
	     try {
	    	  manager.getTransaction().begin();
	    	  Produit produit=manager.find(Produit.class, Integer.parseInt(id));
		      
		      produit.set_nom((String) request.getParameter("nom"));
		      produit.set_modele((String) request.getParameter("modele"));
		      String p=(String) request.getParameter("prix");
		     
		      produit.set_prix(Float.parseFloat(p));
		      p=(String) (String) request.getParameter("quantite");
		      produit.set_quantite(Integer.parseInt(p));
		      produit.setPhoto((String) request.getParameter("photo"));
		      produit.set_iD(Integer.parseInt(id));
		      
		      manager.getTransaction().commit();
		      
		      request.getSession().setAttribute("msg","produit modifié avec succee ! ");
			 response.sendRedirect("Produit?action=list");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("Produit?action=list");
		}
		
	}
}
