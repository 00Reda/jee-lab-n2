package ma.fstt.controller;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ma.fstt.entities.Panier;
import ma.fstt.entities.Produit;
import ma.fstt.entities.Produit_panier;

/**
 * Servlet implementation class panier
 */
@WebServlet("/Panier")
public class PanierControler extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private Gson gson ;
    EntityManager manager;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PanierControler() {
		// TODO Auto-generated constructor stub
        super();
        
        gson = new Gson();
	} 

    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
        manager=emf.createEntityManager();
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	      
		
	    /*  panier panier=new panier(0, "jpa", "jee", "....", 124, 40,"https://picsum.photos/200/300/?random",null);
	      
	     // EntityManagerFactory emf =
	     //         (EntityManagerFactory)getServletContext().getAttribute("emf");
	      
	      EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	      EntityManager manager=emf.createEntityManager();
	     try {
	    	 manager.getTransaction().begin();
		      manager.persist(panier);
		      manager.getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
		}*/
		
		String action=request.getParameter("action");
		if(action==null) action="list";
		switch (action) {
		case "add":
			      this.add(request,response);
			break;
		case "Add":
				  this.add(request,response);
			break;
		case "modify":
			      this.modify(request, response);
			break;
		case "delete":
			 	  this.delete(request, response);
			break;
		case "list":
		      this.list(request, response);
		break;
		case "loadproducts":
		      this.loadproduct(request, response);
		break;
		case "deleteProduct":
		      this.deleteproduct(request, response);
		break;
		case "affectation":
		      this.affectation(request, response);
		break;
		default:
			break;
		}   
	      
	      
	      
		
	}

	private void affectation(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		int id=request.getParameter("id")==null?0:Integer.parseInt(request.getParameter("id"));
		int product=request.getParameter("produit")==null?0:Integer.parseInt(request.getParameter("produit"));
		int quantite=request.getParameter("quantite")==null?1:Integer.parseInt(request.getParameter("quantite"));
		Produit p =new Produit();
		p.set_iD(product);
		
		Panier panier=new Panier();
		panier.set_iD(id);		
		
		Produit_panier pp=new Produit_panier();
		pp.setQuantite(quantite);
		pp.set_produits(p);
		pp.set_paniers(panier);
		ObjectMapper mapper = new ObjectMapper();
		
		 try {
			 EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	         EntityManager em=emf.createEntityManager();
	         em.getTransaction().begin();
	         em.persist(pp);   
	         em.getTransaction().commit();
		      
		    	 String jsonWriter = mapper.writerWithDefaultPrettyPrinter()
		    			 .writeValueAsString("produit ajouté avec succee! ");
			       
			     response.getOutputStream().print(jsonWriter);
			     return ;
		      
		} catch (Exception e) {
			
			e.printStackTrace();
			System.out.println(e.getMessage());
			response.setStatus(400);
			 
		    	 String jsonWriter = mapper.writerWithDefaultPrettyPrinter()
		    			 .writeValueAsString(e.getMessage());
			       
			     response.getOutputStream().print(jsonWriter);
		     return ;
			
		}
	}

	private void deleteproduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		response.setContentType("application/json");
		
		
		try {
	    	 
	    	 EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	         EntityManager em=emf.createEntityManager();
	         
	         long id=request.getParameter("id")==null?0:Integer.parseInt(request.getParameter("id"));
	        
	         
	         em.getTransaction().begin();
	         
	         int c=em.createQuery("delete from Produit_panier p where p.id="+id).executeUpdate();
	         ObjectMapper mapper = new ObjectMapper();
	         String jsonWriter="";
	         
	         if(c>0) {
	        	 
	        	 jsonWriter = mapper.writerWithDefaultPrettyPrinter()
		    			 .writeValueAsString("supprimé avec succee ! ");
	        	 
	         }else {
	        	 
	        	 jsonWriter = mapper.writerWithDefaultPrettyPrinter()
		    			 .writeValueAsString("erreur lors de la supprission ! ");
	        	 
	         }
	    	 
	         em.getTransaction().commit();
             
	    	 
	    	 
		       
		     response.getOutputStream().print(jsonWriter);
		     return ;
		     
		     
		     
		     
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			
			String s=this.gson.toJson(e.getMessage());
			response.setStatus(400);
			response.getOutputStream().print(s);
			
		     return ;
		}
	}

	private void loadproduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		 response.setContentType("application/json");
		
		try {
	    	 
	    	 EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	         EntityManager em=emf.createEntityManager();
	         
	         int id=request.getParameter("id")==null?0:Integer.parseInt(request.getParameter("id"));
	         
	         Panier panier=new Panier();
	         panier.set_iD(id);
	         
	    	 List<Produit_panier>list= em.createQuery("select p from Produit_panier p  where p._paniers= "+id,Produit_panier.class).getResultList();
             
	    	 ObjectMapper mapper = new ObjectMapper();
	    	 String jsonWriter = mapper.writerWithDefaultPrettyPrinter()
	    			 .writeValueAsString(list);
		       
		     response.getOutputStream().print(jsonWriter);
		     return ;
		     
		     
		     
		     
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			
			String s=this.gson.toJson(e.getMessage());
			response.setStatus(400);
			response.getOutputStream().print(s);
			
		     return ;
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private void add(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		
		  
	      
	      if(request.getMethod().equals("GET")) {
	    	   
	    	   request.setAttribute("panier", new Panier());
			   this.getServletContext().getRequestDispatcher("/panier/Add.jsp").forward(request, response);
	    	   return ;
	      }
	      //panier panier =(panier)request.getAttribute("panier");
	      Panier panier=new Panier();
	      Date date=new Date();
	      try {
	    	  date=(new SimpleDateFormat("yyyy-MM-dd")).parse(request.getParameter("date"));
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("Panier?action=list");
			return ;
		}
	      
	      panier.set_date_creation(date);
	                  
	     try {
	    	  manager.getTransaction().begin();
		      manager.persist(panier);   
		      manager.getTransaction().commit();
		      request.getSession().setAttribute("msg","panier ajouter avec succee ! ");
			  response.sendRedirect("Panier?action=list");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("Panier?action=list");
		}
		
	}
	
	private void list(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	     try {
	    	 
	    	 EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	         EntityManager em=emf.createEntityManager();
	    	 ArrayList<Panier>list=(ArrayList<Panier>) em.createQuery("select p from Panier p",Panier.class).getResultList();
		     request.setAttribute("list", list);
		     
		     this.getServletConfig().getServletContext().getRequestDispatcher("/panier/list.jsp").forward(request, response);
		     
		     
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("panier/list.jsp");
		}
		
	}

	
	private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		  
	      Panier panier=new Panier();
	      String id=request.getParameter("id");
	      panier.set_iD(Integer.parseInt(id));
	      
	     try {
	    	  manager.getTransaction().begin();
		    
			
		      panier=manager.merge(panier);
		      manager.remove(panier);
		      request.getSession().setAttribute("msg","panier supprime avec succee !!");
		      
		      manager.getTransaction().commit();
		      
		      
		      ArrayList<Panier>list=(ArrayList<Panier>) manager.createQuery("select p from Panier p",Panier.class).getResultList();
		      request.setAttribute("list", list);
		      this.getServletConfig().getServletContext().getRequestDispatcher("/panier/list.jsp").forward(request, response);
		      
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("Panier?action=list");
		}
		
	}
	
	private void modify(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		  EntityManagerFactory emf =(EntityManagerFactory)this.getServletContext().getAttribute("emf");
	      EntityManager manager=emf.createEntityManager();
	      
	      String id=request.getParameter("id");
	      
	      if(id==null) return;
	      
	      if(request.getMethod().equals("GET")) {
	    	   manager.getTransaction().begin();
	    	   Panier p=manager.find(Panier.class, Integer.parseInt(id));
	    	   request.setAttribute("panier", p);
			   this.getServletContext().getRequestDispatcher("/panier/Add.jsp").forward(request, response);
	    	   return ;
	      }
	      //panier panier =(panier)request.getAttribute("panier");
	      
	                  
	     try {
	    	  manager.getTransaction().begin();
	    	  Panier panier=manager.find(Panier.class, Integer.parseInt(id));
		      
	    	  Date date=new Date();
		      try {
		    	  date=(new SimpleDateFormat("yyyy-MM-dd")).parse(request.getParameter("date"));
				} catch (ParseException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
					request.getSession().setAttribute("error",e.getMessage());
					response.sendRedirect("Panier?action=list");
					return ;
				}
		      panier.set_date_creation(date);
		      manager.getTransaction().commit();
		      
		      
		      request.getSession().setAttribute("msg","panier modifié avec succee ! ");
			 response.sendRedirect("Panier?action=list");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
			request.getSession().setAttribute("error",e.getMessage());
			response.sendRedirect("Panier?action=list");
		}
		
	}
}
