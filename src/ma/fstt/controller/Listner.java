package ma.fstt.controller;


import javax.persistence.*;
import javax.servlet.*;
import javax.servlet.annotation.WebListener;
@WebListener
public class Listner implements ServletContextListener {
	private EntityManagerFactory emFactory;
    // Prepare the EntityManagerFactory & Enhance:
    public void contextInitialized(ServletContextEvent e) {
        
         emFactory =
            Persistence.createEntityManagerFactory("atelier2");
        e.getServletContext().setAttribute("emf", emFactory);
    }
 
    // Release the EntityManagerFactory:
    public void contextDestroyed(ServletContextEvent e) {
    	emFactory = (EntityManagerFactory)e.getServletContext().getAttribute("emf");
		emFactory.close();
    }
}
